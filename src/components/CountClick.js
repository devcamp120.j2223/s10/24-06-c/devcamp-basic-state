import { Component } from "react";

class CountClick extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        }

        // Khai báo với phương thức bind
        // this.clickHandlerEvent = this.clickHandlerEvent.bind(this);
    }

    // Khai báo với phương thức bind
    // clickHandlerEvent() {
    //     this.setState({
    //         count: this.state.count + 1
    //     })
    // }

    // Khai báo không cần phương thức bind
    // Arrow function không có this => this trong arrow function mặc định trỏ ra đối tượng lớn nhất của module là class
    clickHandlerEvent = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    
    render() {
        return (
            <div>
                <button onClick={this.clickHandlerEvent} >Click me!</button>

                <p>Count click: {this.state.count} times</p>
            </div>
        )
    }
}

export default CountClick;